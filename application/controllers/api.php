<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller {

    /*  TEST FUNCTION
    ---------------------------------------- */

    public function test_get()
    {
        // use this URL to test stuff
        $this->response("Stop Testing. Grrrrrrr!!!!!");
    }

    public function teamstest_get()
    {
       $team_data = array(
               'team1' => 'data for team 1' ,
               'team2' => 'data for team 2',
               'team3' => 'data for team 3',
               'team4' => 'data for team 4'
            );
        $this->response($team_data, 200);

       
    }

    public function teams_get()
    {
       

        $this->load->model('api_model');

     //   $teams_data = $this->api_model->get_all_teams();

       $this->load->helper('url');

      //  $product_id = $this->uri->segment(3, 0);
        //$this->uri->segment(3, 0);

        
            $teams_data = $this->api_model->updateC();
            $this->response($teams_data, 200);
 
    }

    public function new_entry_post(){
        // POST for event.
        // lets you create a new event. Reads event_name / start_time / end_time and returns event_id.
            $this->load->model('api_model');

          //  $this->api_model->addscore();

            $new_entry_data = array(
               'team_name' => $this->post('team_name') ,
               'idea' => $this->post('idea'),
               'is_live' => 'Y'
            );

            $this->db->insert('score', $new_entry_data);

            $this->response($new_entry_data, 200);
    }

   public function winners_get(){
        // POST for event.
        // lets you create a new event. Reads event_name / start_time / end_time and returns event_id.
        

            $this->load->model('api_model');

         //   $teams_data = $this->api_model->get_all_teams();

           $this->load->helper('url');

   
            $teams_data = $this->api_model->get_winners();
            $this->response($teams_data, 200);
    }

    

}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */